package my.first.work;

import java.util.Scanner;

public class HomeTask {
    private static long result = 1;
    private static String numbersArray[] = {"zero","one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    private static long factorial(int n){
        if (n>1) result *= n;
        return  (n <= 1) ? result: factorial(--n);
    }

    private static int fibonacci(int n){
        if (n<1) return 0;
        if (n == 1 || n == 2 ) return 1;
        return fibonacci(n-1) + fibonacci(n-2) ;
    }

    public static long fibonacci2(int n){
        if (n<1) return 0;
        if (n == 1 || n == 2) return 1;
        long a=1, b=1, fibonacci=1;
        for(int i= 3; i<= n; i++){
            fibonacci = a + b;
            a = b;
            b = fibonacci;

        }
        return fibonacci;
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true){
            //menu
            System.out.println("Input \"1\" to use factorial (n!)");
            System.out.println("Input \"2\" to use Fibonacci sequence (1..n)");
            System.out.println("Input \"3\" to use number naming");
            System.out.println("Input \"stop\" for finish");
            System.out.println("Input \"up\" to come back to the beginning");
            System.out.print("\nChoose an action: ");
            if (sc.hasNextInt()) {
                int i = sc.nextInt();
                sc.nextLine();
                if (i == 1){ //factorial
                    while (true){
                        System.out.print("\nInput integer: ");
                        if (sc.hasNextInt()) {
                            int n = sc.nextInt();
                            sc.nextLine();
                            System.out.println( n+ "! = " + factorial(n));
                            result = 1;
                        } else {
                            String line =sc.nextLine();
                            if (line.equalsIgnoreCase("stop")) return;
                            if (line.equalsIgnoreCase("up")) break;
                            System.out.println(line+" - this is non-integer value");
                        }
                    }
                }
                else if (i == 2){ // Fibonacci
                    while (true){
                        System.out.print("\nInput integer: ");
                        if (sc.hasNextInt()) {
                            int n = sc.nextInt();
                            sc.nextLine();
                            System.out.print("fibonacci:");
                            for (int k = 0; k < n; k++){
                                System.out.print(fibonacci2(k) + " ");
                            }
                        } else {
                            String line =sc.nextLine();
                            if (line.equalsIgnoreCase("stop")) return;
                            if (line.equalsIgnoreCase("up")) break;
                            System.out.println(line+" - this is non-integer value");
                        }
                    }
                } else if (i == 3){ //number naming
                    while (true){
                        System.out.print("\nInput integer from 0 to 9: ");
                        if (sc.hasNextInt()) {
                            int n = sc.nextInt();
                            sc.nextLine();
                            if (n > -1 && n < 10){
                                switch (n){
                                    case 0:
                                        System.out.print("zero");
                                        break;
                                    case 1:
                                        System.out.print("one");
                                        break;
                                    case 2:
                                        System.out.print("two");
                                        break;
                                    case 3:
                                        System.out.print(numbersArray[3]);
                                        break;
                                    case 4:
                                        System.out.print(numbersArray[4]);
                                        break;
                                    case 5:
                                        System.out.print(numbersArray[n]);
                                        break;
                                    case 6:
                                        System.out.print("six");
                                        break;
                                }
                                if (n == 7) System.out.print(numbersArray[7]);
                                else if (n == 8) System.out.print("eight");
                                else  if (n == 9) System.out.print(numbersArray[numbersArray.length-1]);
                            } else System.out.println(" out of range (0-9)");

                        } else {
                            String line =sc.nextLine();
                            if (line.equalsIgnoreCase("stop")) return;
                            if (line.equalsIgnoreCase("up")) break;
                            System.out.println(line+" - this is non-integer value");
                        }
                    }
                } else System.out.println("Wrong action");
            } else {
                String line =sc.nextLine();
                if (line.equalsIgnoreCase("stop")) return;
                System.out.println("Try again");
            }
        }

    }
}